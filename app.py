from datetime import datetime
from http import HTTPStatus
from html import escape
from flask import Flask, abort, request, send_from_directory, make_response, render_template
from werkzeug.datastructures import WWWAuthenticate
import flask
from base64 import b64decode
import sys
import apsw
from apsw import Error
from secrets import token_hex
from flask_wtf import CSRFProtect
from bcrypt import gensalt, hashpw
from pyg import pygmentize, cssData
conn = None

# Set up app
app = Flask(__name__)
CSRFProtect(app)
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
app.secret_key = token_hex()

# Add a login manager to the app
import flask_login
from flask_login import login_required, login_user, logout_user, current_user
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass

# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    user = conn.cursor().execute(f'SELECT COUNT(*) FROM users WHERE name= ? ', [user_id])
    if user == 0:
        return

    # For a real app, we would load the User from a database or something
    user = User()
    user.id = user_id
    return user


# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        print(f'Basic auth: {uid}:{passwd}')
        u = users.get(uid)
        if u: # and check_password(u.password, passwd):
            return user_loader(uid)
    elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        print(f'Bearer auth: {auth_params}')
        for uid in users:
            if users[uid].get('token') == auth_params:
                return user_loader(uid)
    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')


@app.get('/search')
def search():
    user = current_user.id
    query = request.args.get('q') or '*'
    typ = request.args.get('type')
    if query == "legitAll":
        stmt = escape(f"SELECT * FROM messages", quote=False)
        c = conn.cursor().execute(stmt)
    elif typ == "int":
        stmt = escape(f"SELECT * FROM messages WHERE id=? AND (recipient IN ('All', ?) OR sender = ?)", quote=False)
        c = conn.cursor().execute(stmt, (int(query), user, user))
    elif query == '*':
        stmt = escape(f"SELECT * FROM messages WHERE recipient IN ('All', ?) OR sender = ?", quote=False)
        c = conn.cursor().execute(stmt, (user, user))
    else:
        stmt = escape(f"SELECT * FROM messages WHERE message GLOB ? AND (recipient IN ('All', ?) OR sender = ?)", quote=False)
        c = conn.cursor().execute(stmt, (query, user, user))
    result = f"Query: {pygmentize(stmt)}\n"
    try:
        rows = c.fetchall()
        result = result + 'Result:\n'
        for id, sender, resip, msg, date in rows:
            pretty_msg = f"Message:\n    {escape(msg, quote=False)}\n    ID: {id}, Sender: {escape(sender, quote=False)}, Resipient: {escape(resip, quote=False)}, Sent at: {date}\n"
            result = f'{result}    {pretty_msg}\n'
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)


@app.route('/send', methods=['POST','GET'])
def send():
    try:
        sender = escape(current_user.id, quote=False)
        recipient = escape(request.args.get('recipient'), quote=False)
        message = escape(request.args.get('message'), quote=False)
        today = datetime.now()
        if not sender or not message:
            return f'ERROR: missing sender or message'
        if not recipient or recipient == "Recipient":
            recipient = "All"
        else:
            t = f'SELECT * FROM users WHERE name = ?'
            res_in_db = conn.cursor().execute(t, [recipient]).fetchone()
            if not res_in_db:
                return f'ERROR: recipient does not exist'
        stmt = escape(f"INSERT INTO messages (sender, recipient, message, timestamp) VALUES (?,?,?,?)", quote=False)
        result = f"Query: {pygmentize(stmt)}\n"
        conn.cursor().execute(stmt, [sender,recipient,message,str(today)])
        return f'{result}ok'
    except Error as e:
        return f'ERROR: {e}'

@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.cursor().execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}

@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp


try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY, 
        sender TEXT NOT NULL,
        recipient TEXT NOT NULL,
        message TEXT NOT NULL,
        timestamp TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS users (
        id integer PRIMARY KEY, 
        name TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS passwords (
        id integer PRIMARY KEY, 
        name TEXT NOT NULL,
        hash TEXT NOT NULL);''')
    if c.execute('SELECT COUNT(*) FROM users').fetchone()[0] == 0:
        c.execute(f'INSERT INTO users (name) VALUES ("s")')
        c.execute(f'INSERT INTO passwords (name, hash) VALUES ("s", "{hashpw("123".encode("utf-8"), gensalt()).decode("utf-8")}")')
        c.execute(f'INSERT INTO users (name) VALUES ("a")')
        c.execute(f'INSERT INTO passwords (name, hash) VALUES ("a", "{hashpw("123".encode("utf-8"), gensalt()).decode("utf-8")}")')
        c.execute(f'INSERT INTO users (name) VALUES ("b")')
        c.execute(f'INSERT INTO passwords (name, hash) VALUES ("b", "{hashpw("123".encode("utf-8"), gensalt()).decode("utf-8")}")')


except Error as e:
    print(e)
    sys.exit(1)
import user
