from app import app, conn, user_loader
from allForms import LoginForm, SignupForm
import flask
from flask import request, render_template
from flask_login import login_required, login_user, logout_user, current_user
from bcrypt import gensalt, hashpw, checkpw

@app.route('/getUsername')
@login_required
def username():
    return current_user.id
    
@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return render_template('index.html')

@app.route('/sendToLogin')
def sendToLogin():
    return flask.redirect(flask.url_for('login'))

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return flask.redirect(flask.url_for('login'))

@app.get('/check_password/')
def check_password(input_pass, db_hash):
    return checkpw(input_pass.encode("utf-8"), db_hash.encode("utf-8"))


@app.route('/login', methods=['GET', 'POST'])
def login():
    
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        csrf = form.csrf_token.data
        username = form.username.data
        password = form.password.data
        u = conn.cursor().execute(f'SELECT 1 FROM users WHERE name = ?', [username]).fetchone()
        if u:
            db_pass = conn.cursor().execute(f'SELECT hash FROM passwords WHERE name = ?', [username]).fetchone()[0]
        if u and check_password(password, db_pass):
            user = user_loader(username)
            # automatically sets logged in session cookie

            login_user(user)

            flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')
    
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index_html'))
    return render_template('./login.html', form=form)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        password_two = form.password_two.data

        u = conn.cursor().execute(f'SELECT 1 FROM users WHERE name=?', [username]).fetchone()

        if u:
            print(f'Username {username} is taken')

        elif password != password_two:
            print('Make sure that passwords match!')
        elif not password_check(password):
            print('password not secure enough')
        else:
            next = flask.request.args.get('next')
            if False and not is_safe_url(next):
                return flask.abort(400)
            hash_pass = hashpw(str(password).encode("utf-8"), gensalt()).decode("utf-8")
            user = f"INSERT INTO users (name) VALUES (?) "
            user_hash = f"INSERT INTO passwords (name, hash) VALUES (?,?) "
            conn.cursor().execute(user, [username])
            conn.cursor().execute(user_hash, (username, hash_pass))
            return flask.redirect(flask.url_for('login'))

    return render_template('./signup.html', form=form)

# taken from geeksforgeeks
def password_check(passwd):
      
    SpecialSym =['$', '@', '#', '%']
    val = True
      
    if len(passwd) < 7:
        print('length should be at least 8')
        val = False
          
    if not any(char.isdigit() for char in passwd):
        print('Password should have at least one numeral')
        val = False
          
    if not any(char.isupper() for char in passwd):
        print('Password should have at least one uppercase letter')
        val = False
          
    if not any(char.islower() for char in passwd):
        print('Password should have at least one lowercase letter')
        val = False
          
    if not any(char in SpecialSym for char in passwd):
        print('Password should have at least one of the symbols $@#')
        val = False
    if val:
        return val