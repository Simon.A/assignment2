(READ THE README IN AN IDE, NOT AS RENDERED ON GIT)

TASK 2A)

Usernames and paswords are stored in plaintext in a disctionary in app.py
only way to make new users is to manualy add them in the disctionary in app.py
JS is writen directly in the html so if anyone where to look at the html they could easaly find any flaws in the JS code
The code isnt very well structured, hard to expand as it does not follow any standard guidelines for factorysation. 
There are function in the code that are unused, it looks to not be an ussue but can become one, and its generally bad to keep unused code just laying araound.
there is no check for if the message you try to send is actually from the user that it says its from.
so impersiantion is easy.
SQL injection in the message is quite easy, with ex. ; DROP message FROM messages ; or something of the sort.
in the JS code the way that it makes new messages appear is by making new HTML objects. This can be abused with sending ex. <button onclick=...> getrecked </button> where (...) can be a fucntion that does whatever the user coded it to do.

All info is send in plaintext if you host in http, hard to host https. (wireshark)


TASK 2B)

In part A i have implemented the following features ontop/replacing the templete we where given:
-Added User and User/password instead of the dictionary with users. two seperate databases for extra securaty
-Added password hashing with help from bcrypt library. This uses a salt for extra securaty.
-Added passord requirements such as: longer then 8, lower/uppercase, need a digit, need a symole.
-Logging in now requires a password to be given, and to match the one in the database.
-Added function to register user with a signup page.
-removed the sender field and instead used the logged in user to determen who sent a message.
-Made it so users can only see the messages that they have sent, are sent to everyone or is sent to them.
-Logout button added to make it so you can logout again after logging in.
-Added clearlog button that clears all loaded messages(only really refreshes the page xD)
-Added textfield and button for looking up messages based on ID(This ID is the primary key in the db)
-Added a button "show all*" that shows all the messages in the db, this is onlt there for testing purposes.
-Added random secret key to the flask app for sessions.
-Added "escape" funtion to printing to screen and adding to database to prevent HTML attacks
-imported CSRFprotect to protect from crosssite attacks.



Usage:
by default there is 3 users: s, a and b. all with password "123". these are there for faster testing purpuses. and would be removed for any public version of the app.
making a new user you need to have a unique name and a password that is typed the same way two times. and the above mentioned requierments
after making a user you can login with the name and password you chose(or with the test users)
now you can just follow the buttons on screen to use the website.
Searching with "*" as input pulls all massages that you have access to otherwise it will pull up a message with the *exact message you searched for.
if the resipient field is left blank or is filled with "recipient" any message will be sent as if it was for every user.
looking up a message by ID will require a int type to work and will cast error otherwise.(also requiers that the message with given ID is addressed to you in some way)



Threat model – who might attack the application?
Anyone that has any intrest and the knowhow of how to to so. (assuming the application is not localhosted, as it currently is. then you would need to take the localhost or get into that computer somehow.)
What can an attacker do? 
-SQL inject (hopefully prevented with the prepaied statments)
-
-with how the JS outputs the queries to the user, an attacker can alter the HTML or add new HTML and call for custom functions. with this attack the attacker can basibly do anything with the progam, dump the source code, print the db's, delete code/files...


What damage could be done (in terms of confidentiality, integrity, availability)?

-With an SQL injection, the credibility and integrity of any message could be destroyed. with a change any of it properties. same goes for the other databases. Also the availability can be affected if a such attack manages to drop a table for ex. users or password. then there would be no way to log in.
- see above



Are there limits to what an attacker can do?
-no? i believe a good enough attackers can do basicly anything to the application


Are there limits to what we can sensibly protect against?
- with me being a student and only getting a week with little to no experience in application security, there is very little that can sensibly be done, Hashing the password and adding prepaited statements for SQL and a little logic for not displaying messages that are not you the user is about what i would assume is sensable. On the other hand if i had alot of experience and alot more time, there are many things to do, such as:
    - follow some protocol for general security(ex. secure by disign or other guidelines.)
    - refractor code. make it more readable, keep only dependant and relates methods in the same document etc.
    - 

What are the main attack vectors for the application?
I think that JS is currently the most easily exploitable part, its derectly in the HTML site after you make a user and log in. so if you know any expliots for any of the funtions there, i think that could be explioted.
What should we do (or what have you done) to protect against attacks?
compaired to the skeleton code we were given, i have added some security by hashing the passwords and adding a seperate database for password and users. I have converted from inline SQL queries to prepaied statments. I refactored what i managed with the time given.

What is the access control model?


How can you know that you security is good enough? (traceability)
I have tried to SQL inject and it no longer works.
can no longer send HTML code that works, with the help of the escape function.

TASK 3)

Do you understand the overall design, based on the documentation and available source code?
    - usage instruction are pretty clear, code overview is good.

Do you see any potential improvements that could be made in design / coding style / best practices? I.e., to make the source code clearer and make it easier to spot bugs.
    - Could be split into some more files. ex. one file for pygmentation of messages, one for send/search and one for user related methods.
    - Add some tags for what funtions do, outside for the well names method names. ex. #login method checks if the user and the users SHA256 password match with user dict, then logs the user in.

Based on the threat model described by the author(s) (from part 2B), do you see any vulnerabilities in the application?
    - has secured for SQL injection, but it was still easy to get into the message database even without a user to log in with. (not notisable from the Threat model!?)
    - No mention of cross-site attacks. could be detramental to confidentiality and integrity.
    - Hashing password with no salt is a bad idea, based on that all SHA256 with same input gives same output. so i crosscheck for most common passwords would still give alot of information on passwords of the users of the application.

Do you see any security risks that the author(s) haven't considered? (Have a look at the OWASP Top Ten list and  “Unit 2: Attack Vectors” in Security for Software Engineers)
    - how the messages are updated and redrawen every 100ms is probaly not a good idea for when there are alot of user. This would use alot of performance on the server and for the users machine. (not really a direct decurity risk, but can have impact.)
    - Having a database for users/passwords is fine, but why extract all the users and put them in a dictionary in the python file, and keeping both updated at the same time, isnt the database there for that reason. This takes up dobble the space to store, also saves critical information in more spaces. Not good for security.
    - 
    - these examples are some of the things the author havent considers.
http://localhost:5005/send?sender=test1&message=hacked&recipient=hacker
This sends a message to and from whoever you specify, This happends because the message sending isnt using the logged in user as its sender, it just uses the one spesified in the link.
http://localhost:5005/search?q=*&u=admin (recommend checking admin msg nr 48!!)
get all messages from spesified user. This can also be user to "fish" for all the users. because if i can see all messages one user has access to, i can se who he sendt to and then i can look through thats users messages and find more users that way. I can also just bruteforce check all usernames.

send this as a message and it will make a HTML button on the site, no function execution as far as i can tell. There is probably a way, im just not fermilliar enough til HTML and JS to know if there is a was to inject JS script into the button.
But with a href tag in the button that works. This can easaly send the user to any site, or to any download link or anything like that.
That is probably the most severe security risk left on the application. 

<button type="button" href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">Click me</button>

<script>
      var myFunction = async (x) => {
      console.log(x);
    };
</script>
<button type="button" id="sendBtn2" onclick="myFunction('this is s test')">Click me</button>
Any HTML can be put in and will run as expected.

Looking at the OWASP. There is (as expected of a student) something to say to every point on that list.
A01:2021-Broken Access Control -> can send messages "from" other users to other users.
A02:2021-Cryptographic Failures -> everything is sendt in plaintext. Passwords are sendt to server nonincrypted(and everything else)
A03:2021-Injection -> can inject HTML code for any use (also as if it was from other users)
A04:2021-Insecure Design -> not really sure what i can say about design as im not a security designer.
A05:2021-Security Misconfiguration -> no salt in passwords are not a good idea, saving user info in more then the nesesary places.
A06:2021-Vulnerable and Outdated Components -> Some functions that are unused have not been removed.
A07:2021-Identification and Authentication Failures -> Can send are look at messages at will (only need username of sender or recipient)
A08:2021-Software and Data Integrity Failures -> Same as the other ones. not enough checks for integrity
A09:2021-Security Logging and Monitoring Failures -> No logging outside of logged in user. 
A10:2021-Server-Side Request Forgery -> More of the same.
